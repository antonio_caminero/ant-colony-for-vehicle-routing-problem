from ant import Ant
from customer import Customer
import random

class Vehicle(Ant):
	"""docstring for Vehicle where a vehicle is an ant and a node is a customer"""
	
	def __init__(self, ant_id, initial_node, my_colony_id, capacity):
		super(Vehicle, self).__init__(ant_id, initial_node,my_colony_id)
		self.capacity = capacity
		self.current_quantity = capacity

	def get_decision_table(self, weighted_graph, global_tabu, alpha, beta, q0):
		"""
		Returns the decision table of this ant.
		"""

		decision_table = dict()
		node_neighbors = weighted_graph.neighbors(self.current_node)
		promising_neighbors = list()
		#losers_neighbors = list() #this list is not necessary prob 0.0!!!It's like losers were gone

		for node in node_neighbors:
			if node.quantity <= self.current_quantity:
				if not node in global_tabu:
					promising_neighbors.append(node)
		
		acum = 0.0  #acum is equal to total influence of every promising neighbor
		node_tau_eta = dict()
		max_influence = float("-Inf")
		q = random.random()
		#h is a node which is not in this ant's tabu list nor in tabu global list.
		for h in promising_neighbors:
			edge_current_node_to_h = weighted_graph[self.current_node][h]
			tau_ih = edge_current_node_to_h['pheromone']
			eta_ih = 1.0 / edge_current_node_to_h['weight']
			influence = tau_ih**alpha * eta_ih**beta
			node_tau_eta[h] = influence
			acum += influence
			if q <= q0 and influence > max_influence:
				self.arg_max_influence = h

		if q <= q0 and self.arg_max_influence != None and len(promising_neighbors) != 0: 
			decision_table[self.arg_max_influence] = 1.0
		else:
			for next_node,influence in node_tau_eta.iteritems():
				#influence here is equal to tau_ij**alpha * eta_ij**beta on the paper
				decision_table[next_node] = (influence * 1.0) / acum

		return decision_table


	def update_ant(self,next_node,global_tabu,weighted_graph):
		self.cost += weighted_graph[self.current_node][next_node]['weight']
		global_tabu.add(next_node)
		self.current_quantity -= next_node.quantity
		self.visited_nodes.append(next_node)
		self.current_node = next_node
		self.arg_max_influence = None
	
	def reset_ant(self):
		"""Reset the ant to initial state."""
		#self.current_node = Customer(0,0)
		self.current_quantity = self.capacity
		self.working = True
		self.cost = 0
		self.tabu = set()
		self.visited_nodes = list()
		

	def __str__(self):

		str_visited_nodes = ""
		for cust in self.visited_nodes:
			if cust != self.visited_nodes[-1]:
				str_visited_nodes += "(Customer id: " + str(cust.customer_id) + ",Quantity: "\
				+ str(cust.quantity) + ") -> "
			else:
				str_visited_nodes += "(Customer id: " + str(cust.customer_id) + ",Quantity: "\
				+ str(cust.quantity) +")"

		str_vehicle = "Vehicle id (Ant id): " + str(self.ant_id) + "\n"
		str_vehicle += "My colony id: " + str(self.my_colony_id) + "\n"
		str_vehicle += "Working: " + str(self.working) + "\n"
		str_vehicle += "Cost: " + str(self.cost) + "\n"
		str_vehicle += "Visited nodes: " + str_visited_nodes + "\n"
		str_vehicle += "Capacity: " + str(self.capacity) + "\n"
		str_vehicle += "Current quantity: " + str(self.current_quantity) + "\n"
		str_vehicle += "Tabu list: " + str(self.tabu) + "\n"
		str_vehicle += "Current node(" + self.current_node.__class__.__name__ +"):\n" + \
		str(self.current_node).replace("Customer","\tCustomer").replace("Quantity","\tQuantity")
		return str_vehicle



if __name__ == "__main__":
	v1 = Vehicle(0,0,0,100)
	v2 = Vehicle(0,0,0,100)
	print hash(v1)
	print hash(v2)
