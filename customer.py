class Customer():
	"""Customer class for CVRP"""
	def __init__(self, customer_id,quantity):
		self.customer_id = customer_id
		if quantity < 0:
			raise ValueError("Quantity of goods required by the customer can't be negative.")
		else:
			self.quantity = quantity

	def __eq__(self,other):
		if type(self) is type(other):
			return self.customer_id == other.customer_id

	def __hash__(self):
		return hash(self.__str__())

	def __str__(self):
		str_customer = "Customer id: " + str(self.customer_id) + "\n"
		str_customer += "Quantity: " + str(self.quantity) + " goods\n"
		return str_customer


