# README #
IACO_CVRP
=========

Improved Ant Colony Optimization for solving Capacitated Vehicle Routing Problem

Requisitos
==========
-> python 2.7.x (x>=5)

-> networkx (libreria para el manejo de grafos) networkx >=1.9[1]

-> matplotlib (opcional para la visualizacion de grafos y graficas) matplotlib>=1.3.1[1]

Para instalar networkx en sistemas GNU/Linux abrir un terminal y ejecutar:
"sudo pip install networkx". Donde "pip" [2] es el instalador de paquetes de python. 
Tambien se puede instalar manualmente https://pypi.python.org/pypi/networkx/

Para instalar matplotlib en sistemas GNU/Linux abrir un terminal y ejecutar:
"sudo pip install matplotlib". Donde "pip" [2] es el instalador de paquetes de python. 
Tambien se puede instalar manualmente http://matplotlib.org/1.3.0/users/installing.html



[1]En principio la retrocompatibilidad se garantiza y pip lo instalara en la version mas 
reciente disponible en su repositorio

[2]Para instalar pip en sistemas GNU/Linux basados en debian abrir una terminal y 
hacer "sudo apt-get install python-pip". Tambien se puede obtener siguiendo las siguientes instrucciones que ese indican en https://pip.pypa.io/en/latest/installing.html

Getting started
===============

En el directorio "VRP Problems" se encuentran los 14 problemas que se presentan en el articulo.
Por defecto se puede testear el comportamiento del algoritmo en el fichero test.py haciendo

"python test.py" (Asegurate que el comando python ejecuta la version del interprete
adecuado)

La funcion que se ejecutara sera "main()". Configurar la variable problem_file para cargar
el problema deseado.

Si esta instalada la libreria para las funciones graficas por defecto se dibujara la solucion y el coste se ira imprimiendo por pantalla cada vez que mejora.