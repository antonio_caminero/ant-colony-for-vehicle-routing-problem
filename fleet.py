from vehicle import Vehicle
from customer import Customer
from colony import Colony

class Fleet(Colony):

	def __init__(self,num_of_vehicles,cvrp,vehicle_capacity,fleet_id=0):
		super(Fleet, self).__init__(num_of_vehicles,cvrp,fleet_id)
		self.ants = [Vehicle(vehicle_id,Customer(0,0),fleet_id,vehicle_capacity) for vehicle_id in range(num_of_vehicles)]
		self.operative_vehicles = len(self.ants)
		self.global_tabu = set([Customer(0,0)])

	def step(self):
		"""For each ant this function contructs a decision table and select the next node
			according its probability of being chosen.
		"""
		for vehicle in self.ants:
			if vehicle.working:
				decision_table = vehicle.get_decision_table(self.aco_problem.graph_cvrp,self.global_tabu,self.aco_problem.alpha,self.aco_problem.beta,self.aco_problem.q0)
				if len(decision_table) == 0:
					vehicle.update_ant(Customer(0,0),self.global_tabu,self.aco_problem.graph_cvrp)
					self.operative_vehicles -= 1
					vehicle.working = False
				else:
					next_node = vehicle.select_next_node(decision_table)
					vehicle.update_ant(next_node,self.global_tabu,self.aco_problem.graph_cvrp)

	def route_construction(self):
		"""Construct a solution for the given problem.
		"""
		while self.operative_vehicles > 0:
			self.step()

		self.aco_problem.solution_cost = 0.0
		self.aco_problem.solution = list()
		for ant in self.ants:
			self.aco_problem.solution_cost += ant.cost
			self.aco_problem.solution.append(ant.visited_nodes[:])


	def update_pheromone(self):
		"""Updates pheromone for each edge in graph.
		"""
		global_pheromone_increment = 1.0*self.ants[0].capacity/(len(self.ants)*self.aco_problem.solution_cost)
		all_visited_edges = set()

		for ant in self.ants:
			num_customers_route = len(ant.visited_nodes) - 1
			denominator = num_customers_route * ant.cost

			for i,j in zip(range(len(ant.visited_nodes)),range(1,len(ant.visited_nodes)+1)):
				customer_1 = ant.visited_nodes[i]
				customer_2 = ant.visited_nodes[j%len(ant.visited_nodes)]
				edge = self.aco_problem.graph_cvrp[customer_1][customer_2]
				edge_weight = edge['weight']
				numerator = (ant.cost - edge_weight) * 1.0				
				local_pheromone_increment = numerator/denominator
				pheromone_increment = global_pheromone_increment * local_pheromone_increment
				old_pheromone = edge['pheromone']
				new_pheromone = self.aco_problem.rho * old_pheromone + pheromone_increment

				if new_pheromone < self.aco_problem.tau_min:
					new_pheromone = self.aco_problem.tau_min
				elif new_pheromone > self.aco_problem.tau_max:
					new_pheromone = self.aco_problem.tau_max
				
				self.aco_problem.graph_cvrp[customer_1][customer_2]['pheromone'] = new_pheromone
				all_visited_edges.add((customer_1,customer_2))

		for edge in self.aco_problem.graph_cvrp.edges(data=True):
			if not (edge[0],edge[1]) in all_visited_edges:
				old_pheromone = edge[2]['pheromone']
				new_pheromone = self.aco_problem.rho * old_pheromone
				if new_pheromone < self.aco_problem.tau_min:
					new_pheromone = self.aco_problem.tau_min
				else:
					self.aco_problem.graph_cvrp[edge[0]][edge[1]]['pheromone'] = new_pheromone

	def reset_colony(self):
		"""Resets the colony to initial state."""
		for ant in self.ants:
			ant.reset_ant()
		self.operative_vehicles = len(self.ants)
		self.global_tabu = set([Customer(0,0)])

	def __str__(self):
		str_col = "Fleet id: (Colony id) " + str(self.colony_id) + "\n"
		str_col += "Number of Vehicles: " + str(len(self.ants)) + "\n"
		return str_col