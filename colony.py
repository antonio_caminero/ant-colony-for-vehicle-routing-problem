from ant import Ant
from abc import abstractmethod

class Colony(object):

	def __init__(self, num_of_ants, aco_problem, colony_id=0):
		self.colony_id = colony_id
		self.num_of_ants = num_of_ants
		if num_of_ants <= 0:
			raise ValueError("Number of ants have to be great than 0.")
		self.ants = list()
		self.aco_problem = aco_problem

	@abstractmethod
	def reset_colony(self):
		"""Abstract method to reset the colony's state."""
		pass

	def __eq__(self,other):
		if type(self) is type(other):
			return self.colony_id == other.colony_id

	def __hash__(self):
		return hash(self.__str__())

	def __str__(self):
		str_col = "Colony id: " + self.colony_id + "\n"
		str_col += "Number of ants: " + str(len(self.num_of_ants)) + "\n"
		return str_col


