import os
import math
import networkx as nx
import re
from customer import Customer

def model_problem(filename, random=False, verbose=False):
	""" Models a problem from a plain text file."""
	if not random:
		old_dir = os.getcwd()
		cur_dir = old_dir + "/VRP Problems/" + filename

		problem_txt = open(cur_dir)

		line = problem_txt.readline()
		line = readline_processing(line)
		vehicles, customers = int(line[1]), int(line[2])

		line = problem_txt.readline()
		line = readline_processing(line)
		capacity = int(line[1])

		lines = problem_txt.readlines()
		customers_obj = list()
		customers_coor = dict()

		g = nx.Graph()

		for line in lines:
			line = readline_processing(line)

			if line != ['']:
				customer_id, customer_quantity = int(line[0]),int(line[4])
				customer_x, customer_y = int(line[1]),int(line[2])
				customers_coor[customer_id]=(customer_x,customer_y)
				
				customer = Customer(customer_id,customer_quantity)
				customers_obj.append(customer)

				g.add_node(customer)
		
		for customer in customers_obj:
			customer_id = customer.customer_id

			for neighbor in customers_obj:
				neighbor_id = neighbor.customer_id

				if customer_id != neighbor_id:
					distance = math.sqrt((customers_coor[customer_id][0]-customers_coor[neighbor_id][0])**2 
										+ (customers_coor[customer_id][1]-customers_coor[neighbor_id][1])**2)
					if distance != 0.0:
						g.add_edge(customer,neighbor,{'weight':distance})
					else:
						g.add_edge(customer,neighbor,{'weight':float('Inf')})

		return g,vehicles,capacity

	else:

		pass


def readline_processing(line):

	line = str.strip(line,'\n')
	line = str.strip(line)
	line = re.sub(' +',' ',line)
	line = line.split(' ')

	return line