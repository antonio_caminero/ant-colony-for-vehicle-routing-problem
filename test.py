from problem_modeller import model_problem
from iaco_cvrp import IacoCvrp
from fleet import Fleet
from customer import Customer
import time
import networkx as nx
import math
import random

try:
    import matplotlib.pyplot as plt
    flag_matplot =  True
except ImportError,e:
	print e ,  "\nWARNING: drawing functions won't work"
	flag_matplot =  False




def get_stats(list_values):
	average =  (sum(list_values) * 1.0) / len(list_values)
	deviation = 0.0
	for value in list_values:
		deviation += (value - average)**2.0
	deviation = math.sqrt(deviation * 1.0 / (len(list_values)-1))
	return (average,deviation)


def plot_dict(l1, l2, str1, str2, title):
	plt.plot(l1,l2,'-o')
	plt.title(title)
	plt.xlabel(str1)
	plt.ylabel(str2)
	plt.show()
	

def draw_graph(solution,graph_cvrp,print_edge_label=False):
	G = nx.Graph()
	for route in solution:
		for (cust_i,cust_j) in zip(route,route[1:]):
			G.add_edge(cust_i,cust_j,weight=round(graph_cvrp[cust_i][cust_j]["weight"],2))
		G.add_edge(route[-1],route[0],weight= round(graph_cvrp[route[-1]][route[0]]["weight"],2))

	pos = nx.spring_layout(G)
	nx.draw_networkx_labels(G,pos,font_size=6,font_family='sans-serif')

	nx.draw_networkx_nodes(G,pos,node_size=150)
	nx.draw_networkx_nodes(G,pos,nodelist = [Customer(0,0)],node_size=300,node_color='green')

	nx.draw_networkx_edges(G,pos,edgelist=G.edges(),width=3)	

	if print_edge_label:
			nx.draw_networkx_edge_labels(G,pos,edges_label=G.edges(data=True),label_pos=0.3,font_size=6)

	plt.axis('off')
	plt.show()


def stats():
	graph_cvrp, num_of_vehicles, vehicle_capacity = model_problem("p01.txt")
	list_times = list()
	list_costs = list()

	for _ in range(1):
		iteration = list()
		value = list()
		alpha = 2#random.uniform(1,10) 
		beta =  5#random.uniform(1,10)
		#print "alpha = " + str (alpha), "beta = " + str (beta)
		rho = 0.8
		tau0 = 1.0
		num_of_generations = 250
		q0 = 0.0
		iaco_problem = IacoCvrp(graph_cvrp,num_of_vehicles,vehicle_capacity,alpha,beta,rho,tau0,num_of_generations,q0)
		colony = Fleet(num_of_vehicles,iaco_problem,vehicle_capacity)
		init = time.time()
		aux_time = 0.0
		while iaco_problem.current_generation <= num_of_generations:
			colony.route_construction()
			if iaco_problem.check_mutation():
				#print "Hace mutacion"
				iaco_problem.do_mutation(colony.ants)
			"""print "ANTESSSSSSSSSSSS",colony
			for a in colony.ants:
				print a"""
			iaco_problem.local_search(colony.ants)
			"""print "DESPUESSSSSSSSSSSSSS",colony
			for a in colony.ants:
				print a"""
			colony.update_pheromone()



			#print time.time() - init
			if iaco_problem.update_best_solution():
				#for route in iaco_problem.solution:
				#	print [c.customer_id for c in route]
				iteration.append(iaco_problem.current_generation) 
				value.append(iaco_problem.best_solution_cost)
				aux_time += (time.time() - init)

			colony.reset_colony()

			iaco_problem.current_generation += 1

		#plot_dict(iteration,value,"Iterations","Cost","Taillard problem C7 n=75 Q=140 V = 13 (First approach)")
		list_times.append(aux_time)
		list_costs.append(iaco_problem.best_solution_cost)

	list_times.sort()
	list_costs.sort()
	aux = get_stats(list_costs)
	print "Cost -> ", "Worst: " + str(list_costs[-1]),"Average: " + str(aux[0]), "Best: " + str(list_costs[0]) , "Deviation: " + str(aux[1])
	aux = get_stats(list_times)
	print "Time -> ", "Average: " + str(aux[0]),  "Deviation: " + str(aux[1])




def main():
	graph_cvrp, num_of_vehicles, vehicle_capacity = model_problem("p01.txt")

	alpha = 2.0
	beta =  5.0
	rho = 0.9
	tau0 = 1.0
	num_of_generations = 250
	q0 = 0.0

	iaco_problem = IacoCvrp(graph_cvrp,num_of_vehicles,vehicle_capacity,alpha,beta,rho,tau0,num_of_generations,q0)
	colony = Fleet(num_of_vehicles,iaco_problem,vehicle_capacity)
	while iaco_problem.current_generation <= num_of_generations:
		
		colony.route_construction()
		
		if iaco_problem.check_mutation():
			iaco_problem.do_mutation(colony.ants)
		
		iaco_problem.local_search(colony.ants)
		colony.update_pheromone()

		if iaco_problem.update_best_solution():
			for route in iaco_problem.solution:
				print [c.customer_id for c in route]

		colony.reset_colony()

		iaco_problem.current_generation += 1

	if flag_matplot:
		draw_graph(iaco_problem.best_solution,graph_cvrp)




if __name__ == "__main__":
	main()

	








