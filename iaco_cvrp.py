from colony import Colony
from abc import abstractmethod
from customer import Customer
import random
import math

class IacoCvrp(object):

	def __init__(self, graph_cvrp, num_of_vehicles, vehicle_capacity, alpha, beta, rho, tau0,
		num_of_generations, q0):
		
		if rho <= 0 or rho > 1:
			raise ValueError("Evaporation coefficient \"rho\" is out of range.")

		if q0 < 0 or q0 > 1:
			raise ValueError("Decision parameter \"q0\" is out of range.")
		
		customers_quantity = [customer.quantity for customer in graph_cvrp.nodes()]
		#print sum(customers_quantity)
		if (num_of_vehicles > len(graph_cvrp.nodes()) or num_of_vehicles*vehicle_capacity < sum(customers_quantity) 
			or customers_quantity.count(0)!=1 or num_of_vehicles < 1):
			raise ValueError("There is no solution for this problem with the current arguments.")
		
		self.alpha = alpha
		self.beta = beta
		self.q0 = q0
		self.rho = rho
		self.graph_cvrp = graph_cvrp
		self.customers = graph_cvrp.nodes()
		self.num_of_vehicles = num_of_vehicles 
		self.vehicle_capacity = vehicle_capacity
		self.solution = list()
		self.solution_cost = float("Inf")
		self.best_solution = list()
		self.best_solution_cost = float("Inf")
		self.num_of_generations = num_of_generations
		self.current_generation = 0
		self.print_constants = False
		self.tau0 = tau0
		acum = 0.0
		
		for base_to_i in graph_cvrp.edges_iter(Customer(0,0),data=True):
			acum += base_to_i[2]["weight"]
		self.tau_min = self.vehicle_capacity / (2.0 * acum)
		self.tau_max = self.vehicle_capacity / (1.0 * acum)

		if self.tau0 > self.tau_max:
			self.tau0 = self.tau_max
		elif self.tau0 < self.tau_min:
			self.tau0 = self.tau_min
		
		for edge in graph_cvrp.edges():
			self.graph_cvrp.add_edge(edge[0],edge[1],{'pheromone':self.tau0})

		self.p_mut_min = 1.0 / (len(self.customers)-1)
		self.p_mut_max = 1.0 / self.num_of_vehicles

	def check_mutation(self):
		temp = (self.p_mut_max - self.p_mut_min)
		return random.random() <= self.p_mut_min + temp**(1.0 - self.current_generation*1.0/self.num_of_generations)

	def do_mutation(self, list_of_ants):
		#print "SISII muto"
		parent_solution = self.solution
		if len(parent_solution) >= 2:
			capacity_constraint = False
			while not capacity_constraint:
				indices_mutating_routes = random.sample(xrange(len(parent_solution)),2)
				mutant_index_1 = random.choice(xrange(len(parent_solution[indices_mutating_routes[0]]) - 1))
				mutant_index_2 = random.choice(xrange(len(parent_solution[indices_mutating_routes[1]]) - 1))
				ant_1 = list_of_ants[indices_mutating_routes[0]]
				ant_2 = list_of_ants[indices_mutating_routes[1]]
				customer_1 = ant_1.visited_nodes[mutant_index_1]
				customer_2 = ant_2.visited_nodes[mutant_index_2]
				net_value = abs(customer_1.quantity - customer_2.quantity)
				
				if customer_1.quantity < customer_2.quantity:
					if ant_1.current_quantity - net_value >= 0:
						capacity_constraint = True
						ant_1.current_quantity -= net_value
						ant_2.current_quantity += net_value
				else:
					if ant_2.current_quantity - net_value >= 0:
						capacity_constraint = True
						ant_1.current_quantity += net_value
						ant_2.current_quantity -= net_value
				
				if capacity_constraint:
					self.swap_list_elements(mutant_index_1,mutant_index_2,[ant_1.visited_nodes,ant_2.visited_nodes])
					temp_list = [self.solution[indices_mutating_routes[0]],self.solution[indices_mutating_routes[1]]]
					self.swap_list_elements(mutant_index_1,mutant_index_2,temp_list)

					old_cost_1 = ant_1.cost
					old_cost_2 = ant_2.cost

					ant_1.cost -= self.graph_cvrp[customer_1][ant_1.visited_nodes[mutant_index_1 + 1]]["weight"]
					ant_1.cost -= self.graph_cvrp[customer_1][ant_1.visited_nodes[mutant_index_1 - 1]]["weight"]
					ant_1.cost += self.graph_cvrp[customer_2][ant_1.visited_nodes[mutant_index_1 + 1]]["weight"]
					ant_1.cost += self.graph_cvrp[customer_2][ant_1.visited_nodes[mutant_index_1 - 1]]["weight"]

					
					ant_2.cost -= self.graph_cvrp[customer_2][ant_2.visited_nodes[mutant_index_2 + 1]]["weight"]
					ant_2.cost -= self.graph_cvrp[customer_2][ant_2.visited_nodes[mutant_index_2 - 1]]["weight"]
					ant_2.cost += self.graph_cvrp[customer_1][ant_2.visited_nodes[mutant_index_2 + 1]]["weight"]
					ant_2.cost += self.graph_cvrp[customer_1][ant_2.visited_nodes[mutant_index_2 - 1]]["weight"]

					self.solution_cost += ant_1.cost + ant_2.cost - (old_cost_1 + old_cost_2)

	def swap_list_elements(self, ind_1, ind_2, list_to_swap):
		list_to_swap[0][ind_1],list_to_swap[1][ind_2] = list_to_swap[1][ind_2],list_to_swap[0][ind_1]

	def local_search(self,list_of_ants):
		"""Does a 2-opt local search to obtain the best local solution."""
		self.solution_cost = 0.0
		final_solution = list()
		for route,ant in zip(self.solution,list_of_ants):
			route_copy = route[:]
			min_change = -1.0
			min_i = 0
			min_j = 0
			while min_change < 0.0:
				min_change = 0.0
				for i in range(len(route_copy)-2):
					for j in range(i+2,len(route_copy)):
						length = len(route_copy)
						change = self.graph_cvrp[route_copy[i]][route_copy[j]]['weight']
						change += self.graph_cvrp[route_copy[i+1]][route_copy[(j+1)%length]]['weight']
						change -= self.graph_cvrp[route_copy[i]][route_copy[i+1]]['weight']
						change -= self.graph_cvrp[route_copy[j]][route_copy[(j+1)%length]]['weight']
						change = round(change,6)
						if min_change > change:
							min_change = change
							min_i = i
							min_j = j
				route_copy = self.two_opt_swap(route_copy,min_i,min_j)

			final_solution.append(route_copy)
			ant.visited_nodes = route_copy[:]
			route_cost = sum([self.graph_cvrp[x][y]['weight'] for x,y in zip(route_copy,route_copy[1:])])
			self.solution_cost += route_cost
			ant.cost = route_cost
		self.solution = final_solution

	def two_opt_swap(self, route, i, j):
		"""Swaps i node and j node in route list."""
		#print i,((j-i)/2) + 1
		for index in list(xrange(i,i + ((j-i+1)/2))):
			route[index + 1],route[j - (index - i)] = route[j - (index - i)],route[index + 1]
			#print index+1,j - (index - i)
		#raw_input()
		return route

	def update_best_solution(self):
		if self.best_solution_cost > self.solution_cost:
			print "MEJORA! ->  ", self.solution_cost
			self.best_solution_cost = self.solution_cost
			self.best_solution = self.solution
			return True
		else:
			return False

	def __str__(self):

		str_iaco_cvrp = ""
		if self.print_constants:
			str_iaco_cvrp += "alpha: " + str(self.alpha) + "\n"
			str_iaco_cvrp += "beta: " + str(self.beta) + "\n"
			str_iaco_cvrp += "rho: " + str(self.rho) + "\n"
			str_iaco_cvrp += "q0: " + str(self.q0) + "\n"
			str_iaco_cvrp += "tau0: " + str(self.tau0) + "\n"
			str_iaco_cvrp += "Number of generations: " + str(self.num_of_generations) + "\n"
			str_iaco_cvrp += "Number of customers: " + str(len(self.graph_cvrp.number_of_nodes()) - 1) + "\n"
			str_iaco_cvrp += "Number of vehicles: " + str(self.num_of_vehicles) + "\n"
			str_iaco_cvrp += "Vehicles capacity: " + str(self.vehicle_capacity) + "\n"
			str_iaco_cvrp += "Mutation range probability: [" + str(self.p_mut_min) + "," +  str(self.p_mut_max) + "]"
			str_iaco_cvrp += "Pheromone range: [" + str(self.tau_min) + "," +  str(self.tau_max) + "]"
		else:
			str_iaco_cvrp += "Solution: " + str(self.solution) + "\n"
			str_iaco_cvrp += "Solution cost: " + str(self.solution_cost) + "\n"
			str_iaco_cvrp += "Current generation: " + str(self.current_generation) + "\n"

		return str_iaco_cvrp
