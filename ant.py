import random
from abc import abstractmethod

class Ant(object):
	"""Ant class for IACO"""
	
	def __init__(self, ant_id, initial_node, my_colony_id):
		self.ant_id = ant_id
		self.current_node = initial_node
		self.my_colony_id = my_colony_id
		self.working = True
		self.cost = 0
		self.tabu = set()
		self.visited_nodes = list()
		self.arg_max_influence = None

	@abstractmethod
	def get_decision_table(self, weighted_graph):
		"""
		Abstract method to calculate the decision table.It must be implemented in subclass
		"""
		pass

	def select_next_node(self, decision_table):
		"""
		Returns the next node chosen by this ant according to nodes_probability and 
		"weighted lucky wheel method". Obviously, the higher probability of node j is, the higher 
		probability of this ant selects node j.
		"""

		cumulative_probability = 0.0
		rand_number = random.random()  # This ant spins the wheel

		for node,probability in decision_table.iteritems():
			cumulative_probability += probability
			if cumulative_probability >= rand_number:
				return node
			

	@abstractmethod
	def reset_ant(self):
		"""Abstract method to reset the ant's state."""
		pass

	@abstractmethod
	def update_ant(self, next_node):
		"""Abstract method to update the ant's state"""
		pass

	def __eq__(self,other):
		if type(self) is type(other):
			return self.ant_id == other.ant_id

	def __hash__(self):
		return hash(self.__str__())

	def __str__(self):
		str_ant = "Ant id: " + str(self.ant_id) + "\n"
		str_ant += "Current node:\n\t" + str(self.current_node)
		str_ant += "Tabu list: " + str(self.tabu) + "\n"
		return str_ant